package com.ozu.hypercat.ontology;


public class OntologyConstants {

	public final static String IOT_SERVER_PATH = "/Users/emre/Documents/workspace/IoT/IoTServer";
	public final static String OUTPUT_TYPE = "hasOutputType:";
	public final static String LAST_EVENT_DATE_TIME = "http://cs.ozyegin.edu.tr/muratsensoy/2014/12/sspn#hasEventDateTime";
	public final static String PRODUCED_OUTPUT = "http://cs.ozyegin.edu.tr/muratsensoy/2014/12/sspn#producedOutput";
	public final static String IS_PRODUCED_BY = "http://purl.oclc.org/NET/ssnx/ssn#isProducedBy";

}
