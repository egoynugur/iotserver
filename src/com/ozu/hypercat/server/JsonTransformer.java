package com.ozu.hypercat.server;

import spark.ResponseTransformer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ozu.hypercat.json.gson.extras.RuntimeTypeAdapterFactory;
import com.ozu.hypercat.objects.HypercatCatalogue;
import com.ozu.hypercat.objects.HypercatItem;
import com.ozu.hypercat.objects.HypercatObject;

public class JsonTransformer implements ResponseTransformer {

	private Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	 
	@Override
    public String render(Object model) {
    	GsonBuilder builder = new GsonBuilder();
    	builder.excludeFieldsWithoutExposeAnnotation();
    	
    	RuntimeTypeAdapterFactory<HypercatObject> rta = RuntimeTypeAdapterFactory.of(
    		    HypercatObject.class)
    		    .registerSubtype(HypercatCatalogue.class).registerSubtype(HypercatItem.class);
    	builder.registerTypeAdapterFactory(rta);
        return gson.toJson(model);
    }
}
